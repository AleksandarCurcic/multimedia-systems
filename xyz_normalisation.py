import open3d as o3d
import os

def xyz_normalization(path:str):
    
    pathname, extension = os.path.splitext(path)
    filename = pathname.split('/')
    
    ply = o3d.io.read_point_cloud(path)
    ply.estimate_normals()
    # Outliers
    cl, ind = ply.remove_statistical_outlier(nb_neighbors=20,std_ratio=2.0)
    # Save as .xyz and .ply file
    xyz_filepath = f'{filename[-1]}{extension}'
    ply_filepath = f'{filename[-1]}.ply'
    
    o3d.visualization.draw_geometries([cl])
    o3d.io.write_point_cloud(xyz_filepath, cl)
    o3d.io.write_point_cloud(ply_filepath, cl)
    
    
if __name__ == "__main__":
    xyz_normalization('C:/Users/User/Desktop/MM_SISTEMI/xyz/xyz/obj_113.xyz')