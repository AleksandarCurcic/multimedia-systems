bl_info = {
    "name": "BioNucleus",
    "blender": (3,0,0),
    "category": "Object"
}

import bpy
from bpy import context
import glob
import math
import random
import os

class BioNucleusAddOn(bpy.types.Operator):
    
    bl_idname = 'object.bio_nucleus'
    bl_label = 'Bio Nucleus'
    bl_options = {'REGISTER', 'UNDO'}
    

    number: bpy.props.IntProperty(name='number', default=0, min=0, max=10)
    
    def filter_files(self, path:str):
        xyz_files = glob.glob(f'{path}*.xyz')
        ply_files = glob.glob(f'{path}*.ply')
        all_files = xyz_files + ply_files
        
        return all_files
    
    def execute(self, context):
        files = self.filter_files('C:/Users/User/Desktop/MM_SISTEMI/xyz/xyz/')
        lenght_of_files = len(files)
        random_idxs = random.sample(range(lenght_of_files), self.number)
        random_files = [files[idx] for idx in random_idxs]    
       
        xyz_collection = bpy.data.collections.new('XYZ')
        bpy.context.scene.collection.children.link(xyz_collection)
        xyz_objects = bpy.data.collections['XYZ']

        ply_collection = bpy.data.collections.new('PLY')
        bpy.context.scene.collection.children.link(ply_collection)
        ply_objects = bpy.data.collections['PLY']
               
        ch_collection = bpy.data.collections.new('CH')
        bpy.context.scene.collection.children.link(ch_collection)
        ch_objects = bpy.data.collections['CH']
        
        for file in random_files:
            pathname, extension = os.path.splitext(file)
            if extension == '.xyz':
                with open(file, 'r') as file:
                    lines = file.readlines()
                    all_values = lines[0].split(" ")
                    all_values = [float(x) for x in all_values]
                    
                    x = all_values[0]
                    y = all_values[1]
                    z = all_values[2]
                    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
                    cube = bpy.context.active_object                    
                    cube.select_set(True)
                    bpy.ops.object.editmode_toggle()                    
                    line_counter = 0
                    
                    for line in lines:
                        if line_counter == 99:
                            all_values = line.split(" ")
                            all_values = [float(x) for x in all_values]
                            
                            x = all_values[0]
                            y = all_values[1]
                            z = all_values[2]
                            bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
                            line_counter = 0
                            
                        else:
                            line_counter += 1
                            
                    bpy.ops.object.editmode_toggle()
                    cube.select_set(False)
                    bpy.ops.collection.objects_remove_all()
                    xyz_objects.objects.link(cube)
                    bpy.context.scene.collection.objects.unlink(cube)
                    
                    
                            
            else:

                bpy.ops.import_mesh.ply(filepath=file)
                ply = bpy.context.active_object
                ply_copy = ply.copy()
                
                ply.select_set(True)
                bpy.ops.object.editmode_toggle()
                bpy.ops.mesh.convex_hull()
                bpy.ops.object.editmode_toggle()
                bpy.ops.collection.objects_remove_all()
                
                ch_objects.objects.link(ply)
                ply.select_set(False)
                
               
                bpy.ops.collection.objects_remove_all()
                ply_objects.objects.link(ply_copy)
                

            
        return {'FINISHED'}
    

def menu_func(self, context):
    self.layout.operator(BioNucleusAddOn.bl_idname)


def register():
    bpy.utils.register_class(BioNucleusAddOn)
    bpy.types.VIEW3D_MT_object.append(menu_func)
    

def unregister():
    bpy.utils.unregister_class(BioNucleusAddOn)
    
if __name__ == "__main__":
    register()